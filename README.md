# TRIUMF - MDS Capstone 2019 - Data readiness demonstration

In particle physics jargon, datasets are traditionally divided into _runs_ which are themselves a collection of independent _events_.
We follow the same terminology here. 

The objective is to classify events into three distinct classes ("particle identification"):
 * Muon like,
 * Pion like,
 * Positron like.

For each event we provide:
 * 32 selected event features,
 * a 30 by 30 pixels imprint of the particle on the face of the _calorimeter_.

## Data Sets

The data is provided in HDF5 format. Each file subdivided into three _dataset_ labeled `Muon`, `Pion` and `Positron`. 
 
Within a dataset, every row contains:
 * a vector of 32 floats (named `feature_vector`),
 * a 30 by 30 array of floats (named `cluster_image`).

The table below summaries the number of examples per class and the data set structure.

| Run  | Muon    | Pion   | Positron | Size (MB) | File name        |  
| ---- | ------- | ------ | -----    | --------- | ---------------- |
| 6431 | 2003592 | 549896 | 39084    | 352       | capstone_6431.h5 |
| 6342 | 1083312 | 312693 | 21128    | 194       | capstone_6342.h5 |
| 6356 | 1594559 | 465467 | 31959    | 286       | capstone_6356.h5 |
| 6291 | 889965  | 257521 | 17453    | 160       | capstone_6291.h5 |
| 6501 | 1031157 | 302907 | 21010    | 186       | capstone_6501.h5 |
| 6614 | 859473  | 261144 | 17274    | 157       | capstone_6614.h5 |

### Variables 

#### `feature_vector`

This is an array of 32 floats:

| Index | Name               |
| ----- | ------------------ |
| 0     | Momentum           |
| 1     | LKrEnergy          |
| 2     | LKrHadronicEnergy	 |
| 3     | MUV1Energy         |
| 4     | MUV2Energy         |
| 5     | LKrRMS             |
| 6     | MUV1Weight         |
| 7     | MUV2Weight         |
| 8     | HACWeight          |
| 9     | LKrSeedEnergy      |
| 10    | MUV1SeedEnergy     |
| 11    | MUV2SeedEnergy     |
| 12    | LKrNCells          |
| 13    | MUV1NHits          |
| 14    | MUV2NHits          |
| 15    | LKrInnerEnergy     |
| 16    | MUV1InnerEnergy    |
| 17    | MUV2InnerEnergy    |
| 18    | MUV1SW             |
| 19    | MUV2SW             |
| 20    | HACSW              |
| 21    | Px                 |
| 22    | Py                 |
| 23    | Pz                 |
| 24    | PosAtLKr_X         |
| 25    | PosAtLKr_Y         |
| 26    | LKrDist            |
| 27    | MUV1Dist           |
| 28    | MUV2Dist           |
| 29    | LKrRMS_Asym        |
| 30    | NMUV3              |
| 31    | IsControl          |

#### `cluster_image`

This is 30 by 30 array where each pixel represent a calorimeter "cell" in the XY plane. The pixel value (float) encode the energy deposited in that particular cell.

## A Few Examples


```python
import h5py
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
```


```python
def plot_cluster(cls_img,feat_vec):
    X, Y = np.meshgrid(range(cls_img.shape[0]+1), range(cls_img.shape[0]+1))

    fig, ax = plt.subplots()
    im = ax.pcolormesh(X, Y, cls_img,
                       norm=colors.LogNorm(),
                       cmap='plasma')
    cbar = fig.colorbar(im, ax=ax)
    cbar.set_label('E (MeV/c)')

    ax.set_xlabel('X (cell)')
    ax.set_ylabel('Y (cell)')

    props = dict(boxstyle='round', facecolor='white', alpha=0.5)
    ax.text(0.05, 0.95,
            'p = {p:.2f} MeV/c\nE = {E:.2f} MeV\nMUV1W = {MUV1W:.2f}'.format(p=feat_vec[0],
                                                                             E=feat_vec[1],
                                                                             MUV1W=feat_vec[2]),
            transform=ax.transAxes,
            fontsize=14,
            verticalalignment='top',
            bbox=props)
```


```python
data = h5py.File('capstone_6614.h5','r')
```


```python
#Plot the first five pion like events
for pion in data['Pion'][0:5]:
    plot_cluster(pion['cluster_image'],pion['feature_vector'])
```


![png](output_4_0.png)



![png](output_4_1.png)



![png](output_4_2.png)



![png](output_4_3.png)



![png](output_4_4.png)
